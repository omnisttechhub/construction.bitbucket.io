import React, {
  Component
} from 'react';
// import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div class="banner">
            <img class="ban-img" src="images/banner.png" alt="" />
            <div class="banner-cont">
                <img class="logo" src="images/logo.png" alt="" /> <span>Company Name</span>
                <h1>Construction At Its Best</h1>
            </div>
            <a href="#next"><img class="down_arrow" src="images/Down.png" alt="" /></a>
        </div>
        <section>
            <div class="container">
                <h2>Steps of Construction Process</h2>
                <img class="tab_img" src="images/tab.png" alt="" />
            </div>
        </section>
        <div class="const_area" id="next">
            <div class="container">
                <button>STEP 1</button>
                <h3>Prepare Construction Site</h3>
                <img class="const_img" src="images/img.png" alt="" />
                <h4>1. Prepare construction site and pour foundation</h4>
                <p>Chocolate cake cheesecake danish donut. Dessert halvah tootsie roll sweet roll croissant cake pudding. Ice cream carrot cake cookie chocolate cake sweet roll marzipan. Pudding jelly-o lollipop jelly-o donut marzipan wafer.<br/><br/>
                 Caramels tart chupa chups chocolate bar sesame snaps toffee jelly-o. Chocolate chocolate tootsie roll. Chupa chups dessert donut cookie chocolate bar candy canes. Icing sesame snaps biscuit pastry.
                </p>
            </div>
            <img class="footer_img" src="images/Illus.png" alt="" />
        </div>
      </div>
    );
  }
}

export default App;